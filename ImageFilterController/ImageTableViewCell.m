/*
 
 
 GPUIMAGE_FALSECOLOR
 GPUIMAGE_TONECURVE---1
 GPUIMAGE_TONECURVE---2
 GPUIMAGE_SEPIA
 GPUIMAGE_AMATORKA
 GPUIMAGE_MISSETIKATE
 GPUIMAGE_SOFTELEGANCE
 GPUIMAGE_COLORINVERT
 GPUIMAGE_GRAYSCALE
 GPUIMAGE_THRESHOLD
 GPUIMAGE_PIXELLATE---1
 GPUIMAGE_PIXELLATE---2
 
 GPUIMAGE_POLKADOT---1
 GPUIMAGE_POLKADOT---2
 
 GPUIMAGE_HALFTONE
 GPUIMAGE_XYGRADIENT
 GPUIMAGE_CGA
 GPUIMAGE_DILATION
 GPUIMAGE_OPENING
 GPUIMAGE_CLOSING
 GPUIMAGE_UIELEMENT
 
 */
//
//  ImageTableViewCell.m
//  ImageFilterController
//
//  Created by parkyoseop on 2016. 9. 8..
//  Copyright © 2016년 Yoseop. All rights reserved.
//

#import "ImageTableViewCell.h"

@implementation ImageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setFilterType:(int)aFilterType
{
    filterType = aFilterType;
    switch (aFilterType)
    {
        case GPUIMAGE_FALSECOLOR:
        {
            _filterTitle = @"False Color";
            
            filter1 = [[GPUImageFalseColorFilter alloc] init];
            filter2 = [[GPUImageFalseColorFilter alloc] init];
        }; break;
        case GPUIMAGE_TONECURVE1:
        {
            _filterTitle = @"Tone curve";
            
            filter1 = [[GPUImageToneCurveFilter alloc] init];
            [(GPUImageToneCurveFilter *)filter1 setBlueControlPoints:[NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0.0, 0.0)], [NSValue valueWithCGPoint:CGPointMake(0.5, 0.35)], [NSValue valueWithCGPoint:CGPointMake(1.0, 0.75)], nil]];
            
            filter2 = [[GPUImageToneCurveFilter alloc] init];
            [(GPUImageToneCurveFilter *)filter2 setBlueControlPoints:[NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0.0, 0.0)], [NSValue valueWithCGPoint:CGPointMake(0.5, 0.35)], [NSValue valueWithCGPoint:CGPointMake(1.0, 0.75)], nil]];
        }; break;
        case GPUIMAGE_TONECURVE2:
        {
            _filterTitle = @"Tone curve";
            
            filter1 = [[GPUImageToneCurveFilter alloc] init];
            [(GPUImageToneCurveFilter *)filter1 setBlueControlPoints:[NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0.0, 0.0)], [NSValue valueWithCGPoint:CGPointMake(0.5, 0.65)], [NSValue valueWithCGPoint:CGPointMake(1.0, 0.75)], nil]];
            
            filter2 = [[GPUImageToneCurveFilter alloc] init];
            [(GPUImageToneCurveFilter *)filter2 setBlueControlPoints:[NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0.0, 0.0)], [NSValue valueWithCGPoint:CGPointMake(0.5, 0.65)], [NSValue valueWithCGPoint:CGPointMake(1.0, 0.75)], nil]];
        }; break;
        case GPUIMAGE_SEPIA:
        {
            _filterTitle = @"Sepia Tone";
            filter1 = [[GPUImageSepiaFilter alloc] init];
            [(GPUImageSepiaFilter *)filter1 setIntensity:0.6];
            
            filter2 = [[GPUImageSepiaFilter alloc] init];
            [(GPUImageSepiaFilter *)filter2 setIntensity:0.6];
        }; break;
        case GPUIMAGE_AMATORKA:
        {
            _filterTitle = @"Amatorka (Lookup)";
            
            filter1 = [[GPUImageAmatorkaFilter alloc] init];
            filter2 = [[GPUImageAmatorkaFilter alloc] init];
        }; break;
        case GPUIMAGE_MISSETIKATE:
        {
            _filterTitle = @"Miss Etikate (Lookup)";
            
            filter1 = [[GPUImageMissEtikateFilter alloc] init];
            filter2 = [[GPUImageMissEtikateFilter alloc] init];
        }; break;
        case GPUIMAGE_SOFTELEGANCE:
        {
            _filterTitle = @"Soft Elegance (Lookup)";
            
            filter1 = [[GPUImageSoftEleganceFilter alloc] init];
            filter2 = [[GPUImageSoftEleganceFilter alloc] init];
        }; break;
        case GPUIMAGE_COLORINVERT:
        {
            _filterTitle = @"Color Invert";
            
            filter1 = [[GPUImageColorInvertFilter alloc] init];
            filter2 = [[GPUImageColorInvertFilter alloc] init];
        }; break;
        case GPUIMAGE_GRAYSCALE:
        {
            _filterTitle = @"Grayscale";
            
            filter1 = [[GPUImageGrayscaleFilter alloc] init];
            filter2 = [[GPUImageGrayscaleFilter alloc] init];
        }; break;
        case GPUIMAGE_THRESHOLD:
        {
            _filterTitle = @"Luminance Threshold";
            
            filter1 = [[GPUImageLuminanceThresholdFilter alloc] init];
            filter2 = [[GPUImageLuminanceThresholdFilter alloc] init];
        }; break;
        case GPUIMAGE_PIXELLATE1:
        {
            _filterTitle = @"Pixellate";
            filter1 = [[GPUImagePixellateFilter alloc] init];
            [(GPUImagePixellateFilter *)filter1 setFractionalWidthOfAPixel:0.15*0.1];
            
            filter2 = [[GPUImagePixellateFilter alloc] init];
            [(GPUImagePixellateFilter *)filter2 setFractionalWidthOfAPixel:0.15*0.1];
        }; break;
        case GPUIMAGE_PIXELLATE2:
        {
            _filterTitle = @"Pixellate";
            filter1 = [[GPUImagePixellateFilter alloc] init];
            [(GPUImagePixellateFilter *)filter1 setFractionalWidthOfAPixel:0.3*0.1];
            
            filter2 = [[GPUImagePixellateFilter alloc] init];
            [(GPUImagePixellateFilter *)filter2 setFractionalWidthOfAPixel:0.3*0.1];
        }; break;
        case GPUIMAGE_POLKADOT1:
        {
            _filterTitle = @"Polka Dot";
            
            filter1 = [[GPUImagePolkaDotFilter alloc] init];
            [(GPUImagePolkaDotFilter *)filter1 setFractionalWidthOfAPixel:0.15*0.1];
            
            filter2 = [[GPUImagePolkaDotFilter alloc] init];
            [(GPUImagePolkaDotFilter *)filter2 setFractionalWidthOfAPixel:0.15*0.1];
        }; break;
        case GPUIMAGE_POLKADOT2:
        {
            _filterTitle = @"Polka Dot";
            
            filter1 = [[GPUImagePolkaDotFilter alloc] init];
            [(GPUImagePolkaDotFilter *)filter1 setFractionalWidthOfAPixel:0.3*0.1];
            
            filter2 = [[GPUImagePolkaDotFilter alloc] init];
            [(GPUImagePolkaDotFilter *)filter2 setFractionalWidthOfAPixel:0.3*0.1];
        }; break;
        case GPUIMAGE_HALFTONE:
        {
            _filterTitle = @"Halftone";
            
            filter1 = [[GPUImageHalftoneFilter alloc] init];
            [(GPUImageHalftoneFilter *)filter1 setFractionalWidthOfAPixel:0.1*0.1];
            
            filter2 = [[GPUImageHalftoneFilter alloc] init];
            [(GPUImageHalftoneFilter *)filter2 setFractionalWidthOfAPixel:0.1*0.1];
        }; break;
        case GPUIMAGE_XYGRADIENT:
        {
            _filterTitle = @"XY Derivative";
            
            filter1 = [[GPUImageXYDerivativeFilter alloc] init];
            filter2 = [[GPUImageXYDerivativeFilter alloc] init];
        }; break;
        case GPUIMAGE_CGA:
        {
            _filterTitle = @"CGA Colorspace";
            
            filter1 = [[GPUImageCGAColorspaceFilter alloc] init];
            filter2 = [[GPUImageCGAColorspaceFilter alloc] init];
        }; break;
        case GPUIMAGE_DILATION:
        {
            _filterTitle = @"Dilation";
            
            filter1 = [[GPUImageRGBDilationFilter alloc] initWithRadius:4];
            filter2 = [[GPUImageRGBDilationFilter alloc] initWithRadius:4];
        }; break;
        case GPUIMAGE_OPENING:
        {
            _filterTitle = @"Opening";
            
            filter1 = [[GPUImageRGBOpeningFilter alloc] initWithRadius:4];
            filter2 = [[GPUImageRGBOpeningFilter alloc] initWithRadius:4];
        }; break;
        case GPUIMAGE_CLOSING:
        {
            _filterTitle = @"Closing";
            
            filter1 = [[GPUImageRGBClosingFilter alloc] initWithRadius:4];
            filter2 = [[GPUImageRGBClosingFilter alloc] initWithRadius:4];
        }; break;
        case GPUIMAGE_UIELEMENT:
        {
            _filterTitle = @"UI Element";
            
            filter1 = [[GPUImageSepiaFilter alloc] init];
            filter2 = [[GPUImageSepiaFilter alloc] init];
        }; break;
        default: {
            filter1 = [[GPUImageSepiaFilter alloc] init];
            filter2 = [[GPUImageSepiaFilter alloc] init];
        }break;
    }
    
    // ================================================================================
    // ================================================================================
    // ================================================================================
    // ================================================================================
    // ================================================================================
    // ================================================================================
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSLog(@"--------------- %@",_filterTitle);
        UIImage *inputImage1 = [UIImage imageNamed:@"testImage"];
        stillImageSource1 = [[GPUImagePicture alloc] initWithImage:inputImage1];
        [stillImageSource1 addTarget:filter1];
        [filter1 useNextFrameForImageCapture];
        [stillImageSource1 processImage];
        UIImage *filteredImage1 = [filter1 imageFromCurrentFramebuffer];
        
        UIImage *inputImage2 = [UIImage imageNamed:@"testImage3"];
        stillImageSource2 = [[GPUImagePicture alloc] initWithImage:inputImage2];
        [stillImageSource2 addTarget:filter2];
        [filter2 useNextFrameForImageCapture];
        [stillImageSource2 processImage];
        UIImage *filteredImage2 = [filter2 imageFromCurrentFramebuffer];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imageView1.image = filteredImage1;
            self.imageView1.clipsToBounds = YES;
            
            self.imageView2.image = filteredImage2;
            self.imageView2.clipsToBounds = YES;
            
            self.rowLabel.text = [@(aFilterType) stringValue];
            self.filterNameLabel.text = _filterTitle;
        });
    });
}

@end
