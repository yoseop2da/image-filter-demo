//
//  ImageTableViewCell.h
//  ImageFilterController
//
//  Created by parkyoseop on 2016. 9. 8..
//  Copyright © 2016년 Yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"

typedef enum {
    GPUIMAGE_FALSECOLOR,
    GPUIMAGE_TONECURVE1,
    GPUIMAGE_TONECURVE2,
    GPUIMAGE_SEPIA,
    GPUIMAGE_AMATORKA,
    GPUIMAGE_MISSETIKATE,
    GPUIMAGE_SOFTELEGANCE,
    GPUIMAGE_COLORINVERT,
    GPUIMAGE_GRAYSCALE,
    GPUIMAGE_THRESHOLD,
    GPUIMAGE_PIXELLATE1,
    GPUIMAGE_PIXELLATE2,
    GPUIMAGE_POLKADOT1,
    GPUIMAGE_POLKADOT2,
    GPUIMAGE_HALFTONE,
    GPUIMAGE_XYGRADIENT,
    GPUIMAGE_CGA,
    GPUIMAGE_DILATION,
    GPUIMAGE_OPENING,
    GPUIMAGE_CLOSING,
    GPUIMAGE_UIELEMENT
} GPUImageShowcaseFilterType;


@interface ImageTableViewCell : UITableViewCell
{
    GPUImageShowcaseFilterType filterType;
    GPUImageOutput<GPUImageInput> *filter1;
    GPUImageOutput<GPUImageInput> *filter2;
    GPUImagePicture *sourcePicture;
    GPUImageUIElement *uiElementInput;
    GPUImageFilterPipeline *pipeline;
    GPUImagePicture *stillImageSource1;
    GPUImagePicture *stillImageSource2;
    //    UIImage *currentFilteredVideoFrame;
    
    NSString *_filterTitle;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageView1;
@property (weak, nonatomic) IBOutlet UIImageView *imageView2;

@property (weak, nonatomic) IBOutlet UILabel *rowLabel;
@property (weak, nonatomic) IBOutlet UILabel *filterNameLabel;

- (void)setFilterType:(int)aFilterType;


@end
