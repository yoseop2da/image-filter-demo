//
//  ViewController.m
//  ImageFilterController
//
//  Created by parkyoseop on 2016. 9. 8..
//  Copyright © 2016년 Yoseop. All rights reserved.
//

#import "ViewController.h"
#import "ImageTableViewCell.h"

@interface ViewController()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 21;
}

- (ImageTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ImageTableViewCell class])];
    [cell setFilterType:(int)indexPath.row];
    return cell;
}

@end
